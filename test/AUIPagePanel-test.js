import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import AUIPagePanel from '../src/AUIPagePanel';

describe('AUIPagePanel', () => {
    it('should render the correct AUI markup', () => {
        expect(render(<AUIPagePanel />).html()).to.equal(`<div class="aui-page-panel"><div class="aui-page-panel-inner"><section class="aui-page-panel-content content-body"></section></div></div>`);
    });

    it('should render an aside container if an aside is provided', () => {
        expect(render(<AUIPagePanel aside={<div>Hello World</div>} />).html()).to.equal(`<div class="aui-page-panel"><div class="aui-page-panel-inner"><section class="aui-page-panel-content content-body"></section><div class="aui-page-panel-sidebar content-sidebar"><div>Hello World</div></div></div></div>`);
    });

    it('should render a nav if a nav is provided', () => {
        expect(render(<AUIPagePanel nav={<div>Hello World</div>} />).html()).to.equal(`<div class="aui-page-panel"><div class="aui-page-panel-inner"><div class="aui-page-panel-nav"><div>Hello World</div></div><section class="aui-page-panel-content content-body"></section></div></div>`);
    });
});
