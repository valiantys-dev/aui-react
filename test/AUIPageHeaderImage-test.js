import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import AUIPageHeaderImage from '../src/AUIPageHeaderImage';

describe('AUIPageHeaderImage', () => {
    it('should render the correct AUI markup', () => {
        expect(render(<AUIPageHeaderImage />).html()).to.equal(`<div class="aui-page-header-image"><span class="aui-avatar aui-avatar-large aui-avatar-project"><span class="aui-avatar-inner"></span></span></div>`);
    });
});
