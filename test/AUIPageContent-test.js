import React from 'react';
import { render } from 'enzyme';

import { expect } from 'chai';
import AUIPageContent from '../src/AUIPageContent';

describe('AUIPageContent', () => {
    it('should render to correct AUI container', () => {
        expect(render(<AUIPageContent>Hello world</AUIPageContent>).html()).to.equal(`<section id="content" role="main"><div class="aui-page-panel"><div class="aui-page-panel-inner"><section class="aui-page-panel-content content-body">Hello world</section></div></div></section>`);
    });
});
