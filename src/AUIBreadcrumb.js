import React, { PropTypes } from 'react';

const AUIBreadcrumb = props => {
    return (
        <li><a href="#"><span className="aui-nav-item-label">{props.children}</span></a></li>
    );
};

AUIBreadcrumb.displayName = 'AUIBreadcrumb';
AUIBreadcrumb.propTypes = {
    children: PropTypes.element
};

export default AUIBreadcrumb;
