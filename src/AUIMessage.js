import React from 'react';
import classnames from 'classnames';

const AUIMessage = ({type, title, children}) => {
    let typeClass;
    if (type) {
        typeClass = 'aui-message-' + type;
    }
    const classes = classnames('aui-message', typeClass);

    return (
        <div className={classes}>
            {!title ? null : <p className="title"><strong>{title}</strong></p>}
            {children}
        </div>
    );
};

AUIMessage.displayName = 'AUIMessage';
AUIMessage.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.element
    ]).isRequired,
    title: React.PropTypes.string,
    type: React.PropTypes.oneOf(['warning', 'error', 'success', 'hint', 'info'])
};

export default AUIMessage;
