import React, { Component, PropTypes } from 'react';

const AUIDropdownSection = ({ children }) => (
    <div className="aui-dropdown2-section">
        <ul className="aui-list-truncate">
            {children}
        </ul>
    </div>
);

export default AUIDropdownSection;
