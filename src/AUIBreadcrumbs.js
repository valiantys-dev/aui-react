import React, { PropTypes } from 'react';

const AUIBreadcrumbs = props => {
    return (
        <ol className="aui-nav aui-nav-breadcrumbs">
            {props.children}
        </ol>
    );
};

AUIBreadcrumbs.displayName = 'AUIBreadcrumbs';
AUIBreadcrumbs.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
    ])
};

export default AUIBreadcrumbs;
